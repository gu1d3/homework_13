#include <iostream>

void num(int a, int b)
{
    for (int i = b; i < a; i += 2)
    {
        std::cout << i << "\n";
    }
}

int main()
{
    int n;
    std::cout << "Enter number: " << "\n";
    std::cin >> n;
    
    std::cout << "Even: \n";
    num(n, 0);

    std::cout << "Odd: \n";
    num(n, 1);


}

